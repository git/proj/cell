# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="PS3 alternative bootloader"
HOMEPAGE="http://ozlabs.org/~jk/projects/petitboot/"
SRC_URI="http://ozlabs.org/~jk/projects/petitboot/downloads/bin-0.0.1/otheros.bld"

LICENSE=""
SLOT="0"
KEYWORDS="~ppc ~ppc64"
IUSE=""

DEPEND="app-misc/ps3pf_utils"
RDEPEND=""

src_unpack () {
	true
}

src_install () {
	insinto /boot
	doins $DISTDIR/otheros.bld
}

pkg_postinst() {
	einfo "The binary otheros.bld from the petitboot project"
	einfo "was installed to the /boot directory. This version"
	einfo "of petitboot cannot read device names from the "
	einfo "kboot.conf file.  Remove the device name from"
	einfo "kernel label in the conf file before using this flash"
	einfo "image."
	ebeep 5
	einfo " ** USE AT YOUR OWN RISK **"
	ebeep 5
}
