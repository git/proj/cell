# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="A wrapper library to adapt the JSRE SPU usage model to SPUFS"
HOMEPAGE="http://ozlabs.org/pipermail/cbe-oss-dev/"
SRC_URI="http://dev.gentoo.org/~lu_zero/distfiles/${P}.tar.bz2"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~ppc ~ppc64"
IUSE="debug"

DEPEND=""
RDEPEND=""

export CBUILD=${CBUILD:-${CHOST}}
export CTARGET=${CTARGET:-${CHOST}}

if [[ ${CTARGET} == ${CHOST} ]] ; then
	if [[ ${CATEGORY/cross-} != ${CATEGORY} ]] ; then
		export CTARGET=${CATEGORY/cross-}
	fi
fi

if [[ ${CTARGET} == ${CHOST} ]] ; then
	SYSROOT=""
else
	SYSROOT="/usr/${CTARGET}"
fi

src_unpack () {
	unpack ${A}
	#just in case something is missing
}

src_compile() {
	myconf=""
	use debug && myconf="${myconf} DEBUG=1"
	make all elfspe2-all CROSS="${CTARGET}-" prefix=/usr SYSROOT="$SYSROOT" ${myconf}
}

src_install() {
	make CROSS="${CTARGET}-" prefix=/usr SYSROOT="$SYSROOT" \
		 DESTDIR="$D" install elfspe2-install
	cp ${FILESDIR}/spe.rc6 ${T}/elfspe
	doinitd ${T}/elfspe
}

pkg_postinst() {
	einfo "You may want to register elfspe to binfmt using the"
	einfo "provided initscript"
	einfo "# rc-update add elfspe boot"
	ewarn "make sure your fstab contains the following line"
	ewarn "none                    /spu            spufs           defaults
	0 0"
	ewarn "and that you have spufs support enabled in the kernel"
}
