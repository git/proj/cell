# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-fs/iprutils/iprutils-2.2.8.ebuild,v 1.1 2008/01/05 20:13:43 ranger Exp $

inherit eutils

S=${WORKDIR}/${PN}
DESCRIPTION="Binary PS3 Kernel"
SRC_URI="dev.gentoo.org/~ranger/downloads/ps3/${P}.tar.bz2"
HOMEPAGE=""

SLOT="0"
LICENSE="IPL-1"
KEYWORDS="~ppc ~ppc64"
IUSE=""

src_install () {
	insinto /boot/
	doins ${WORKDIR}/kernel-genkernel-ppc-${PV}-ps3
	doins ${WORKDIR}/System.map-genkernel-ppc-${PV}-ps3
	doins ${WORKDIR}/initramfs-genkernel-ppc-${PV}-ps3

	insinto /lib/modules/
	doins -r ${WORKDIR}/2.6.24-ps3/

}

pkg_postinst() {
	einfo "You now need to add an entry to your /etc/kboot.conf"
	einfo "that contains the newly installed binary kernel and "
	einfo "ramdisk.  Use the existing kboot.conf label as an"
	einfo "example.  Be sure to use a different label name for "
	einfo "the new kernel label."
	einfo ""
	einfo "After you reboot and are using this kernel, consider"
	einfo "running depmod."
	einfo ""

	ebeep 5
}
