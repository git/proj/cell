
#K_WANT_GENPATCHES="base extras"
#K_GENPATCHES_VER="15"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
ETYPE="sources"
inherit kernel-2 eutils
detect_version

DESCRIPTION="vanilla plus ps3 patches"
HOMEPAGE=""

PS3_PATCHES_URI="http://dev.gentoo.org/~ranger/downloads/ps3/${PV}-ps3updates.diff.bz2"

SRC_URI="${KERNEL_URI} ${PS3_PATCHES_URI}"

KEYWORDS="ppc ppc64"


src_unpack() {
	kernel-2_src_unpack
	#cd ${S}
	cd ..
	epatch ${DISTDIR}/${PV}-ps3updates.diff.bz2
	epatch ${FILESDIR}/ps3fb-fix-gpu-cmd-buff-size-2.10-linux-2.6.23-20071023.diff


}


pkg_postinst() {
	postinst_sources
}

