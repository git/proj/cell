ETYPE="sources"
inherit kernel-2 eutils
detect_version
 
DESCRIPTION="vanilla plus ps3 patches"
HOMEPAGE=""
 
 
SRC_URI="${KERNEL_URI}"
UNIPATCH_STRICT=1
UNIPATCH_LIST="${FILESDIR}/00-sony-2.6.24.patch.bz2 ${FILESDIR}/99-squashfs-3.3.patch.bz2"
 
KEYWORDS="~ppc ~ppc64"
 
 
pkg_postinst() {
	postinst_sources
}
