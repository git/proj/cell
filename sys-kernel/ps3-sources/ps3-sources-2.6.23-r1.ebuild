ETYPE="sources"
inherit kernel-2 eutils
detect_version
 
DESCRIPTION="vanilla plus ps3 patches"
HOMEPAGE=""
 
PS3_PATCHES_URI="http://dev.gentoo.org/~ranger/downloads/ps3/${PV}-ps3updates.diff.bz2"
 
SRC_URI="${KERNEL_URI} ${PS3_PATCHES_URI}"
UNIPATCH_STRICT=1
UNIPATCH_LIST="${DISTDIR}/${PV}-ps3updates.diff.bz2 ${FILESDIR}/ps3fb-fix-gpu-cmd-buff-size-2.10-linux-2.6.23-20071023.diff"
 
KEYWORDS="ppc ppc64"
 
 
pkg_postinst() {
	postinst_sources
}
