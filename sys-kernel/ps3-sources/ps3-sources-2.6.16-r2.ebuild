
#K_WANT_GENPATCHES="base extras"
#K_GENPATCHES_VER="15"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
ETYPE="sources"
inherit kernel-2 eutils
detect_version

DESCRIPTION="vanilla plus ps3 patches"
HOMEPAGE=""

#CELLPATCHES_URI="http://kernel.org/pub/linux/kernel/people/arnd/patches/${ARND_VER}/${ARND_VER}.diff.bz2"

#PS3_PATCHES_URI="http://dev.gentoo.org/~lu_zero/distfiles/2.6.16-ps3pf2.diff.bz2"


PS3_PATCHES_URI="http://dev.gentoo.org/~ranger/downloads/ps3/${PV}-update.diff.bz2"

SRC_URI="${KERNEL_URI} ${PS3_PATCHES_URI}"

KEYWORDS="ppc ppc64"


src_unpack() {
	kernel-2_src_unpack
	cd ${S}
	epatch ${DISTDIR}/${PV}-update.diff.bz2
	epatch ${FILESDIR}/squashfs3.1-patch.bz2

}


pkg_postinst() {
	postinst_sources
}

